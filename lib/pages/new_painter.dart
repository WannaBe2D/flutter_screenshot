import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_painter/image_painter.dart';
import 'package:path_provider/path_provider.dart';

class ImagePainterExample extends StatefulWidget {
  const ImagePainterExample({Key? key, required this.screnshoot})
      : super(key: key);
  final String screnshoot;
  @override
  _ImagePainterExampleState createState() => _ImagePainterExampleState();
}

class _ImagePainterExampleState extends State<ImagePainterExample> {
  final _imageKey = GlobalKey<ImagePainterState>();
  final _key = GlobalKey<ScaffoldState>();

  void saveImage() async {
    try {
      final image = await _imageKey.currentState?.exportImage();
      final directory = (await getApplicationDocumentsDirectory()).path;
      await Directory('$directory/sample').create(recursive: true);
      final fullPath =
          '$directory/flutter_screenshot/Screenshots/${DateTime.now().millisecondsSinceEpoch}.png';
      final imgFile = File('$fullPath');
      imgFile.writeAsBytesSync(image!);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Save to: $fullPath"),
      ));
      await File(widget.screnshoot).delete();
      // ignore: empty_catches
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      appBar: AppBar(
        title: const Text("Flutter Screenshot"),
        actions: [
          IconButton(
            icon: const Icon(Icons.save_alt),
            onPressed: saveImage,
          )
        ],
      ),
      body: ImagePainter.file(
        File(widget.screnshoot),
        key: _imageKey,
        scalable: true,
        initialStrokeWidth: 2,
        initialColor: Colors.red,
        initialPaintMode: PaintMode.arrow,
      ),
    );
  }
}
